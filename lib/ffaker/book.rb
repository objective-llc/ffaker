module Faker
  module Book
    extend ModuleUtils
    extend self

    def isbn10
      isbn = ""
      (1..10).each do |i|
        isbn += Kernel.rand(10).to_s
      end
      return isbn
    end

    def isbn13
      return "978#{isbn10}"
    end

    def isbn10_with_dashes
      isbn = isbn10
      "#{isbn[0]}-#{isbn[1..4]}-#{isbn[5..8]}-#{isbn[9]}"
    end

    def isbn13_with_dashes
      isbn = isbn13
      "#{isbn[0..2]}-#{isbn[3]}-#{isbn[4..7]}-#{isbn[8..11]}-#{isbn[12]}"
    end

    def title
      [
        "#{TITLE_BEGINNING.rand} #{TITLE_MIDDLE.rand}#{TITLE_END.rand}",
        "#{TITLE_BEGINNING.rand} #{TITLE_MIDDLE.rand}",
        "#{TITLE_MIDDLE.rand}"
      ][Kernel.rand(3)]
    end

    def binding
      BINDINGS.rand
    end

    def edition
      EDITIONS.rand
    end

    def price
      Kernel.rand(10000) / 100.0
    end

    def pages
      Kernel.rand(1000)
    end

    def publication_date
      now = Time.now.to_i
      ten_years_ago = now - (60 * 60 * 24 * 365 * 10)
      date = Time.at(Kernel.rand(now - ten_years_ago) + ten_years_ago)
      Time.mktime(date.year, date.month, date.day)
    end

    def publisher
      publisher = ''
      num_words = Kernel.rand(4) + 1
      num_words.times { |i| publisher += Faker::Lorem.word.capitalize! + ' ' }
      "#{publisher}#{PUBLISHER_SUFFIXES.rand}"
    end

    TITLE_BEGINNING = k ['How to do', 'How to Make', '']

    TITLE_MIDDLE = k ['Gardening', 'Calculus', 'Marketing', 'Balloons',
                      'Creativity', 'Kinesiology', 'Law 101']

    TITLE_END = k [' for Dummies', ' at Home', ' in the Nighttime',
                   ' for Children', ' for Adults', ': a Manifesto',
                   ' in 24 hours', ' in 10 minutes', ' for Mom',
                   ': A Novel']

    BINDINGS = k ['hardcover', 'softcover', 'digital']

    EDITIONS = k ['1st', '2nd', '3rd', 'first', 'second', 'third']

    PUBLISHER_SUFFIXES = k ['Press', 'Editions', 'Books', 'Publishing',
                            'Works', 'Publications', 'Publishers',
                            'Publishing House', 'Publishing Group']
  end
end
